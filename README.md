# A Hangouts user script #

Hangouts in browser shows your video feed (how others see you) at the bottom right. As you change the size of window of the browser, the size remains the same. This is particularly problematic when you reduce the size of the window to the point where your feed covers the one of your correspondent.

This user script allows you to reduce the size of the window and still see your correspondent.

# Setup #

If you have not done it yet, install a [user script extension](https://greasyfork.org/help/installing-user-scripts) to your browser and click the link below to install.

https://bitbucket.org/troquard/hangoutsshrinkmyface/raw/master/%5BHangouts%5D%20Shrink%20My%20Face.user.js