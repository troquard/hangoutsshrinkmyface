// ==UserScript==
// @name         [Hangouts] Shrink My Face
// @namespace    https://bitbucket.org/troquard/
// @version      0.2
// @description  Shrinks my face on Google Hangouts video calls.
// @author       Nicolas Troquard
// @updateURL    https://bitbucket.org/troquard/hangoutsshrinkmyface/raw/master/%5BHangouts%5D%20Shrink%20My%20Face.user.js
// @downloadURL  https://bitbucket.org/troquard/hangoutsshrinkmyface/raw/master/%5BHangouts%5D%20Shrink%20My%20Face.user.js
// @match        https://hangouts.google.com/call/*
// @grant        none
// ==/UserScript==


(function() {
    'use strict';

    // I want my face to be 10 percent the size of the window
    // That should leave me the opportunity to click on my face
    // if I want to see it big for real.
    var VERTICAL_RATIO = 0.10;
    var HORIZONTAL_RATIO = 0.10;

    var oldwwidth = -17; // dummy
    var oldwheight = -17; // dummy

    // we will reassess the adequate size of my face every 10 seconds
    setInterval(function(){

	// this should work for most browsers
	// to find the dimensions of the window
        var wwidth = window.innerWidth ||
	    document.documentElement.clientWidth ||
	    document.body.clientWidth;
        var wheight = window.innerHeight ||
	    document.documentElement.clientHeight ||
	    document.body.clientHeight;

        if (wwidth === oldwwidth && wheight === oldwheight) {
	    // if the size of the window has not changed
	    // my face should be fine; we have nothing to do
            return;
	}
        else {
            oldwwidth = wwidth;
            oldwheight = wheight;
        }

	// we identified "gSTPzb" as the culprit element hosting the big faces
        var culpritelements = document.getElementsByClassName("gSTPzb")
        // these are other elements concerning big faces // Wa //ZEZETe //"p2hjYe zDyG0c" // gSTPzb // Wmf6nf

	// now considering the size of the window
	// that should be a good size for my face
        var newh = Math.round(wheight * VERTICAL_RATIO);
        var neww = Math.round(wwidth * HORIZONTAL_RATIO);

        var elem;
        for (var i = 0 ; i < culpritelements.length ; i++) {
            elem = culpritelements[i];
            if (elem) {
		// bam! new size for my face!
                // window.alert("my face resized!");
                elem.style.height = newh + 'px';
                elem.style.width = neww + 'px';
            }
        }

    }, 10000);

})();